import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators,NgForm} from '@angular/forms'  
import { RegistrationService } from '../../../services/registration.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  rForm: FormGroup;
  post:any;                     // A property for our submitted form
  username:string = '';      //password
  password:string = '';
  email:string;
  role:string='user';
  // titleAlert:string = 'This field is required';
  // ServicesService:any;
  // emailPattern : "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  titleAlert:string = 'This field is required. Minimum length is 3 letters!';
  emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
  passwordAlert:string='Password should have 5 letters';
  constructor(private fb: FormBuilder,private service:RegistrationService,private router: Router, private toastr: ToastrService) 
{ 
  this.rForm = fb.group({
  'username' : ['',[Validators.required, Validators.minLength(3)]],
  'password' : ['',[Validators.required, Validators.minLength(5),]],
  'validate' : '',
  'email': ['', [Validators.required,Validators.email, Validators.pattern(this.emailPattern)]],
  'role':'user',
});

}
ngOnInit() {
  this.rForm.get('validate').valueChanges.subscribe(

    (validate) => {

        if (validate == '1') {
            this.rForm.get('username').setValidators([Validators.required, Validators.minLength(3)]);
            this.titleAlert = 'You need to specify at least 3 characters';
        } else {
            this.rForm.get('username').setValidators(Validators.required);
        }
        this.rForm.get('username').updateValueAndValidity();

    });
}
addPost(post:any) {
  this.service.addPost(post).subscribe((res:any)=>{
   
    console.log("You are registered!::",res);
    console.log('res====',res)
    if(res.code == 200){
      localStorage.setItem('LoggedInUser', 'true');    
      localStorage.setItem('id',res.data._id)
      localStorage.setItem('token', res.data.token);    
      localStorage.setItem("role",res.data.role);
      localStorage.setItem('username',res.data.username)
      // if(res.code==200){alert('SUccess')}
      // else{alert('Fail')}
      this.toastr.success('Registration Successful!');
      this.router.navigate(['/userdashboard']);
    } 
    else if(res.code == 11000){         //code 11000 described in backend as error!
      this.toastr.error('ERROR! Email Id already exists.');
    }
    else {
      // alert(res.msg)
      this.toastr.error('ERROR! Email Id already exists.');
    }
    
  })

}
  get officialEmail() {
    return this.rForm.get('email');
} 
}
