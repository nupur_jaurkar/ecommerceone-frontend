import { Injectable } from '@angular/core';
import {Observable} from 'rxjs'
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http: HttpClient) { }

addPost(data: any): Observable<any>{
    return this.http.post('http://localhost:10010/registration', data)
  }
}
