import { Component, OnInit,  ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { UserService } from '../../../../../services/user.service';
import { ReactiveFormsModule } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-products',
  templateUrl: './edit-products.component.html',
  styleUrls: ['./edit-products.component.css']
})
export class EditProductsComponent implements OnInit {
  rForm: FormGroup;
  post:any; 
  productName:string='';
  productDescription:string='';
  productCost:number;
  productCategory:string='';
  file :null;

  @ViewChild('fileInput') fileInput: ElementRef;

  constructor(private fb: FormBuilder,private route:ActivatedRoute,
    private service:UserService, private router: Router,) {
      this.rForm = fb.group({
        'productName' : [null, Validators.required],
        'productDescription' : [null, Validators.required],
        'productCost' : [null, Validators.required],
        'productCategory' : [null, Validators.required],
        'validate' : '',
        file :null
      });
     }

  ngOnInit() {
  }
  onFileChange(event) {
    if(event.target.files.length > 0) {
      let file = event.target.files[0];
      this.rForm.get('file').setValue(file);
    }
  }
  
  private prepareSave(): any {
    let input = new FormData();
    input.append('productName', this.rForm.get('productName').value);
    input.append('productDescription', this.rForm.get('productDescription').value);
    input.append('productCost', this.rForm.get('productCost').value);
    input.append('productCategory', this.rForm.get('productCategory').value);
    input.append('file', this.rForm.get('file').value);
    return input;
  }

  editProducts(data, id){
  const formModel = this.prepareSave();
  this.route.params.subscribe(params =>{
    this.service.editProducts(params.id,formModel).subscribe((res:any)=>{
      console.log("Edit your blog!::",res)
      if(res.code=200){
        alert(res)
      }
    })
  })
}
}
