import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {
  blogslist:any;

  constructor(private blogs:UserService,private router :Router) { }

  ngOnInit() {
    this.getDetails()  
  }
  getDetails(){
    this.blogs.listBlogs().subscribe(res => {
      this.blogslist = res;
      console.log("blogs",this.blogslist);
    });
  }

}
