import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { HomeModuleRoutingModule } from './home-module-routing.module';
import { BlogsComponent } from './component/blogs/blogs.component';
import { HomepageComponent } from './component/homepage/homepage.component';
import { RegistrationComponent } from './component/registration/registration.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './component/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 
import { ToastrModule } from 'ngx-toastr';
import {CardModule} from 'primeng/card';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {PaginatorModule} from 'primeng/paginator';



@NgModule({
  imports: [
    CommonModule,
    HomeModuleRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    CardModule,
    TableModule,
    ButtonModule,
    PaginatorModule
  ],
  declarations: [BlogsComponent, HomepageComponent, RegistrationComponent, LoginComponent]
})
export class HomeModuleModule { }
