import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { FormGroup, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  productslist:any;

  constructor(private products:UserService,private router :Router,private toastr: ToastrService) { 
    this.rForm= new FormGroup({
      'productName':new FormControl('')
    })
  }
  rForm:FormGroup;


  ngOnInit() {
    this.getDetails() 
  }

  getDetails(){
    this.products.listProducts().subscribe(res => {
      this.productslist = res;
      console.log("products",this.productslist);
    });
  }
  addToCart(products){
    console.log(this.rForm.value)
    var userId = localStorage.getItem("id");      //userId
    this.rForm.value.userId = userId;
    this.rForm.value.productName = products.productName;
    this.rForm.value.productCost = products.productCost;
    this.rForm.value.productId = products._id;
    console.log('cart contents',this.rForm.value);
    this.products.addToCart(this.rForm.value).subscribe(res => {
      // this.productslist = res;
      // console.log("products",this.productslist);
      if(res.code==200){
        this.toastr.success('Product added successfully');
        }
    });
    
    
  }





  
}
