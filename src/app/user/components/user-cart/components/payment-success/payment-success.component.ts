import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-payment-success',
  templateUrl: './payment-success.component.html',
  styleUrls: ['./payment-success.component.css']
})
export class PaymentSuccessComponent implements OnInit {

  constructor(private payment_success:UserService, private router : ActivatedRoute) { }

  ngOnInit() {
    console.log('in payment_success')
    let id = localStorage.getItem('id')
    // this.router.params.subscribe(params=>{
    console.log('userid==',id)
    this.payment_success.payment_success(id).subscribe(res =>
    { 
      console.log('payment successfull')
    })
  // })
  
}

}
