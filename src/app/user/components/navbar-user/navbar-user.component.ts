import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar-user',
  templateUrl: './navbar-user.component.html',
  styleUrls: ['./navbar-user.component.css']
})
export class NavbarUserComponent implements OnInit {
  username: String = localStorage.getItem("username");
  constructor(private router: Router) { }

  ngOnInit() {
  }

  logout() {
    localStorage.removeItem('username');
    localStorage.removeItem('token');
    localStorage.removeItem('LoggedInUser');
    localStorage.removeItem('id');
    localStorage.removeItem('role');
    localStorage.removeItem('blogId');
    localStorage.removeItem('payableAmount');
    this.router.navigate(['/'])

  }
}
