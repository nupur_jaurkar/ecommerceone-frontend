import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { UserService } from '../../../../../services/user.service';
import { ReactiveFormsModule } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-users',
  templateUrl: './edit-users.component.html',
  styleUrls: ['./edit-users.component.css']
})
export class EditUsersComponent implements OnInit {
  rForm: FormGroup;
  post:any;                     // A property for our submitted form
  description:string = '';
  username:string = ''; 
  password:string='';
  email:string='';  
  role:string = '';

  // imagesBlog:string = '';
  // titleAlert:string = 'This field is required';
  // loading: boolean = false;
  // file:null;

  constructor(private fb: FormBuilder,private route:ActivatedRoute,
    private service:UserService, private router: Router,) {
      this.rForm = fb.group({
        'username' : [null, Validators.required],
        'password' : [null, Validators.required],
        'email': [null, Validators.required],
        'role':["admin", Validators.required],
        'validate' : '',
      });
  }

  ngOnInit() {
  }

  editUsers(data, id){
    this.route.params.subscribe(params =>{
      this.service.editUsers(params.id,this.rForm.value).subscribe((res:any)=>{
        console.log("Edit your user!::",res)
        if(res.code=200){
          alert(res)
        }
      })
    })
  }

}
